package ficheros;

import excepciones.ExcepcionClaseNoInstanciada;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Ficheros {
    private FileReader flujoEntrada;
    private BufferedReader filtroEntrada;
    private FileWriter flujoSalida;
    private BufferedWriter filtroSalida;

    private Ficheros() {

    }

    /**
     * Método que crea un fichero con el nombre pasado como argumento
     *
     * @param nombreFichero Nombre del fichero a crear
     * @throws IOException                 Suele lanzar cuando hay problemas de permisos
     * @throws ExcepcionClaseNoInstanciada Lanza cuando la clase no está instanciada
     */
    public void crearFichero(String nombreFichero) throws IOException, ExcepcionClaseNoInstanciada {
        if (this == null)
            throw new ExcepcionClaseNoInstanciada("Ficheros");
        else {
            File fichero = new File(nombreFichero);
            flujoSalida = new FileWriter(fichero);
        }
    }

    /**
     * Método que destruye el fichero con nombre pasado como argumento
     *
     * @param nombreFichero Nombre del fichero qye se debe destruir
     * @throws IOException                 Lanza cuando hay problemas de ficheros
     * @throws FileNotFoundException       Lanza cuando no se encuentra el archivo
     * @throws ExcepcionClaseNoInstanciada Lanza cuando la clase no está instanciada
     */
    public void destruirFichero(String nombreFichero) throws IOException, FileNotFoundException, ExcepcionClaseNoInstanciada {
        if (this == null)
            throw new ExcepcionClaseNoInstanciada("Ficheros");
        else {
            Path rutaFichero = Paths.get("$(pwd)/" + nombreFichero);
            try {
                Files.delete(rutaFichero);
            } catch (FileNotFoundException ficheroNoEncontrado) {
                throw new FileNotFoundException("Fichero no encontrado");
            }
        }
    }


}
