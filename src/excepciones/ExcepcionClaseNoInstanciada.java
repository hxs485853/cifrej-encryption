package excepciones;

public class ExcepcionClaseNoInstanciada extends Exception {

    public ExcepcionClaseNoInstanciada(String clase) {
        super("La clase " + clase + " todavía no se ha instanciado");
    }

}
